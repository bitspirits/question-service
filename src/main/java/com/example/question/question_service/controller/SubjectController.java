package com.example.question.question_service.controller;

import com.example.question.question_service.model.Category;
import com.example.question.question_service.model.Subject;
import com.example.question.question_service.service.category.ICategoryService;
import com.example.question.question_service.service.subject.ISubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api/v1/subject")
public class SubjectController {

    @Autowired
    private ISubjectService service;

    @PostMapping("/save")
    public ResponseEntity<Map<String ,Object>> save(@RequestBody Subject subject){
        return service.save(subject);
    }

    @GetMapping("/all")
    public ResponseEntity<Map<String ,Object>> getAll(){
        return service.getAll();
    }



}
