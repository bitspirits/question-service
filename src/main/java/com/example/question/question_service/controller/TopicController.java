package com.example.question.question_service.controller;

import com.example.question.question_service.model.Subject;
import com.example.question.question_service.model.Topic;
import com.example.question.question_service.service.subject.ISubjectService;
import com.example.question.question_service.service.topic.ITopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api/v1/topic")
public class TopicController {

    @Autowired
    private ITopicService service;

    @PostMapping("/save")
    public ResponseEntity<Map<String ,Object>> save(@RequestBody Topic topic){
        return service.save(topic);
    }

    @GetMapping("/all")
    public ResponseEntity<Map<String ,Object>> getAll(){
        return service.getAll();
    }


}
