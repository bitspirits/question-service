package com.example.question.question_service.controller;

import com.example.question.question_service.model.Section;
import com.example.question.question_service.model.Subject;
import com.example.question.question_service.service.section.ISectionService;
import com.example.question.question_service.service.subject.ISubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api/v1/section")
public class SectionController {
    @Autowired
    private ISectionService service;

    @PostMapping("/save")
    public ResponseEntity<Map<String ,Object>> save(@RequestBody Section section){
        return service.save(section);
    }

    @GetMapping("/all")
    public ResponseEntity<Map<String ,Object>> getAll(){
        return service.getAll();
    }

}
