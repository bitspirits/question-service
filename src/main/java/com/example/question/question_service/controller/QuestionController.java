package com.example.question.question_service.controller;

import com.example.question.question_service.dto.DtoSubjectWithQuestion;
import com.example.question.question_service.model.Category;
import com.example.question.question_service.model.Question;
import com.example.question.question_service.model.Section;
import com.example.question.question_service.service.question.IQuestionService;
import com.example.question.question_service.utils.Constants;
import org.hibernate.annotations.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/question")
public class QuestionController {

    @Autowired
    private IQuestionService service;

    @Autowired
    private HttpServletRequest context;

    @PostMapping("/save")
    public ResponseEntity<Map<String ,Object>> save(@RequestBody Question question){
        return service.save(question);
    }

    @GetMapping("/all")
    public ResponseEntity<Map<String ,Object>> getAll(){
        return service.getAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Map<String ,Object>> getById(@PathVariable("id") Long id){
        return service.findById(id);
    }

    @PostMapping("/filter")
    public  ResponseEntity<Map<String, Object>> filter(@RequestBody Map<String ,String > map){
        return service.filter(context.getAttribute(Constants.KEY_USER_CATEGORY_ID).toString(), map);
    }

    @PostMapping("/question_sheet")
    public  ResponseEntity<Map<String, Object>> questionSheet(@RequestBody List<DtoSubjectWithQuestion> subjectWithQuestions){
        return service.filter(subjectWithQuestions);
    }

//    @GetMapping("/filter")
//    public ResponseEntity<Map<String, Object>> filter(@Param("subject") String subject, @Param("limit") String limit) {
//        return service.filter(context.getAttribute(Constants.KEY_USER_CATEGORY_ID).toString(), subject, limit);
//    }


}
