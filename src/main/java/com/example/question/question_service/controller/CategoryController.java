package com.example.question.question_service.controller;

import com.example.question.question_service.model.Category;
import com.example.question.question_service.service.category.ICategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api/v1/category")
public class CategoryController {

    @Autowired
    private ICategoryService service;

    @PostMapping("/save")
    public ResponseEntity<Map<String ,Object>> save(@RequestBody Category category){
        return service.save(category);
    }

    @GetMapping("/all")
    public ResponseEntity<Map<String ,Object>> getAll(){
        return service.getAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Map<String ,Object>> getCategoryById(@PathVariable("id") String id){
        return service.getCategoryById(id);
    }

}
