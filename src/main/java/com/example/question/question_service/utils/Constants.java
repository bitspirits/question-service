package com.example.question.question_service.utils;

import java.util.ArrayList;

public class Constants {

    public static final String KEY_MESSAGE = "message";
    public static final String KEY_STATUS = "status";
    public static final String KEY_DATA = "data";
    public static final String KEY_TOKEN = "token";

    public static final String TOKEN_SECRET_KEY = "bcs2";
    public static final long TOKEN_VALIDATION = 1 * 365 * 24 * 60 * 60 * 1000;

    public static final String KEY_USER_ID = "id";
    public static final String KEY_USER_CATEGORY_ID = "category_id";
    public static final String KEY_USER_EMAIL = "email";

    private static final ArrayList<String> whitelisted_urls = new ArrayList<>();

    public static ArrayList<String> getWhitelisted_urls() {
        whitelisted_urls.clear();
        whitelisted_urls.add("/api/v1/category/**");
        return whitelisted_urls;
    }
}
