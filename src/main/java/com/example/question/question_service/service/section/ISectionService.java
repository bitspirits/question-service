package com.example.question.question_service.service.section;

import com.example.question.question_service.model.Category;
import com.example.question.question_service.model.Section;
import org.springframework.http.ResponseEntity;

import java.util.Map;

public interface ISectionService {
    ResponseEntity<Map<String ,Object>> save(Section section);
    ResponseEntity<Map<String ,Object>> getAll();
}
