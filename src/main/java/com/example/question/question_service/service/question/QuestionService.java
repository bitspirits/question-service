package com.example.question.question_service.service.question;

import com.example.question.question_service.dto.DtoOption;
import com.example.question.question_service.dto.DtoQuestion;
import com.example.question.question_service.dto.DtoSubjectWithQuestion;
import com.example.question.question_service.model.Category;
import com.example.question.question_service.model.Option;
import com.example.question.question_service.model.Question;
import com.example.question.question_service.model.Subject;
import com.example.question.question_service.repository.IOptionRepository;
import com.example.question.question_service.repository.question.IQuestionRepository;
import com.example.question.question_service.repository.question.QuestionRepository;
import com.example.question.question_service.utils.Constants;
import com.example.question.question_service.utils.ResponseModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Service
public class QuestionService implements IQuestionService {

    @Autowired
    private IQuestionRepository repository;

    @Autowired
    private IOptionRepository optionRepository;

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private HttpServletRequest context;


    @Override
    public ResponseEntity<Map<String, Object>> save(Question question) {

        if (question.getId() != 0 || !repository.findByTitle(question.getTitle()).isPresent()) {
            Question q = repository.save(question);
            q.getCategory().setSubjects(new ArrayList<>());
            q.getSubject().setSections(new ArrayList<>());
            q.getSubject().setCategories(new ArrayList<>());
            q.getSection().setTopics(new ArrayList<>());
            q.getSection().setSubjects(new ArrayList<>());
            q.getTopic().setSections(new ArrayList<>());

            question.getOptions().forEach(option -> {
                option.setQuestion(q);
            });
            List<Option> options = optionRepository.saveAll(question.getOptions());
            options.forEach(option -> option.setQuestion(null));
            q.setOptions(options);

            return new ResponseModel(HttpStatus.OK
                    , "Question save successfully"
                    , true)
                    .setData(q)
                    .build();
        } else {
            return new ResponseModel(HttpStatus.OK
                    , "Question Name Already Exist"
                    , false)
                    .build();
        }
    }


    @Override
    public ResponseEntity<Map<String, Object>> findById(Long id) {

        Optional<Question> optionalQuestion = repository.findById(id);
        Question q = optionalQuestion.get();
        q.getCategory().setSubjects(new ArrayList<>());
        q.getSubject().setSections(new ArrayList<>());
        q.getSubject().setCategories(new ArrayList<>());
        q.getSection().setTopics(new ArrayList<>());
        q.getSection().setSubjects(new ArrayList<>());
        q.getTopic().setSections(new ArrayList<>());
        q.getOptions().forEach(option -> option.setQuestion(null));

        return new ResponseModel(HttpStatus.OK
                , "Question by id"
                , true)
                .setData(repository.findById(id))
                .build();
    }

    @Override
    public ResponseEntity<Map<String, Object>> getAll() {
        List<Question> questions = repository.findAll();
        questions.forEach(question -> {
            question.getCategory().setSubjects(new ArrayList<>());
            question.getSubject().setCategories(new ArrayList<>());
            question.getSubject().setSections(new ArrayList<>());
            question.getSection().setSubjects(new ArrayList<>());
            question.getSection().setTopics(new ArrayList<>());
            question.getTopic().setSections(new ArrayList<>());
            question.getOptions().forEach(option -> option.setQuestion(null));
        });

        return new ResponseModel(HttpStatus.OK
                , "Question all fetch successfully"
                , true)
                .setData(repository.findAll())
                .build();
    }

    @Override
    public ResponseEntity<Map<String, Object>> filter(String categoryId, Map<String, String> map) {

        TypedQuery<Subject> s = entityManager.createQuery("select s from Subject s where s.id IN(" + map.get("subject") + ")", Subject.class);
        List<DtoSubjectWithQuestion> subjectWithQuestions = new ArrayList<>();
        s.getResultList().forEach(subject -> {

            List<DtoQuestion> dtoQuestions = new ArrayList<>();
            TypedQuery<Question> q = entityManager.createQuery("select q from Question q where q.subject = " +
                    subject.getId() + " and q.category.id = :categoryId  ORDER BY random()", Question.class);
            q.setMaxResults(Integer.parseInt(map.get("limit"))).setParameter("categoryId", Long.parseLong(categoryId)).getResultList().forEach(question -> {
                List<DtoOption> options = new ArrayList<>();
                question.getOptions().forEach(option -> {
                    options.add(new DtoOption(option.getId(), option.getAnswer(), option.getCorrect()));
                });

                dtoQuestions.add(new DtoQuestion(
                        question.getId(),
                        question.getTitle(),
                        question.getSection().getId(),
                        question.getSection().getName(),
                        question.getTopic().getId(),
                        question.getTopic().getName(),
                        options));

            });
            subjectWithQuestions.add(new DtoSubjectWithQuestion(subject.getId(), subject.getName(), dtoQuestions));

        });

        return new ResponseModel(HttpStatus.OK
                , "Question all fetch successfully"
                , true)
                .setData(subjectWithQuestions)
                .build();


       /* TypedQuery<DtoQuestion> s = entityManager.createQuery("Select " +
                "NEW com.example.question.question_service.dto.DtoQuestion(" +
                "s, " +
//                "(select q from Question q where q.subject = s.id and q.category = "+ categoryId +")" +
                ""+ categoryId +")" +
                " from Subject s where s.id IN("+ map.get("subject") +")",DtoQuestion.class);
        List<DtoQuestion> questionsWithSubject = s.getResultList();
        ;*/


//        finalQuery = baseQuery+endPointQuery;
//TODO

//        ApplicationUserDetails userDetails = (ApplicationUserDetails)
//                SecurityContextHolder.getContext().getAuthentication().getPrincipal();

//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        if (!(authentication instanceof AnonymousAuthenticationToken)) {
//            String currentUserName = authentication.getName();
//            return currentUserName;
//        }

//        String baseQuery = "select NEW com.example.question.question_service.dto.DtoQuestion(q.title) from Question q where ";
//        String baseQuery = "select q from Question q where ";
//        String endPointQuery = "";
//        String finalQuery = "";
//
//
//        endPointQuery = endPointQuery + "q.category = " +
//                ( categoryId != null
//                 ? categoryId
//                 : "1" );
//
//        endPointQuery = map.containsKey("subject")
//                ? endPointQuery + " and q.subject = " + map.get("subject")
//                : endPointQuery;

       /* endPointQuery = map.containsKey("section")
                ? endPointQuery + " and q.section = " + map.get("section")
                : endPointQuery;

        endPointQuery = map.containsKey("topic")
                ? endPointQuery + " and q.topic = " + map.get("topic")
                : endPointQuery;
*/
        //finalQuery = baseQuery + endPointQuery +" ORDER BY random()";

//        TypedQuery<Question> q =
//                entityManager.createQuery(finalQuery, Question.class);

//
//        TypedQuery<Collection> s = entityManager.createQuery("Select " +
//                "c.subjects from Category c where  c.id = "+categoryId ,Collection.class);
//
//        Collection subjects =  s.getResultList();
//        List<Subject> ss = (List<Subject>) subjects;
//        Query q = entityManager.createQuery(finalQuery);
//        List<Object[]> questions = q.getResultList();
//        TypedQuery<Question> q = entityManager.createQuery("Select q from Question q where q.category IN(1)", Question.class).setMaxResults(1);
//        Query q3 = em.createQuery("Select s from StudentEntity s where s.s_name like '%monim%'");
//        Query q1 = em.createQuery("Select s from StudentEntity s order by s.s_age desc");


//        Query queryCount = entityManager.createQuery("SELECT COUNT(q) FROM Question q");
//        Long size = (Long) queryCount.getSingleResult();

//        List<DtoQuestion> categories = entityManager.createQuery("SELECT NEW com.example.question.question_service.dto.DtoQuestion(ve, v) " +
//                "FROM Question v JOIN v.category ve\n" +
//                "GROUP BY ve\n").getResultList();

//        String customQuery = "SELECT " +
//                "NEW com.example.question.question_service.dto.DtoQuestion(category, " +
//                "(SELECT q FROM Question q WHERE q.category = :categoryID))" +
//                " FROM Category category";
//        TypedQuery<DtoQuestion> cust1 = entityManager.createQuery(customQuery, DtoQuestion.class);
//        List<DtoQuestion> qs1 = cust1.setMaxResults(1).getResultList();
//        List<Question> questions = q.setParameter("indexs", new Random().ints(0, Integer.parseInt(String.valueOf(size)), 1).toArray()).getResultList();

//       String cq = "SELECT   NEW com.example.question.question_service.dto.DtoQuestion(c, (SELECT qq FROM Question qq WHERE q.category = c.id))" +
//               "FROM Category c\n" +
//               "         INNER JOIN\n" +
//               "         Question q\n" +
//               "         ON c.id = q.category\n";
//               "         AND PH.PhoneNumberTypeID = 3\n";

//        String cq = "select c, (select q from Question q where q.category = c.id) from Category c";
        // String cq = "SELECT DISTINCT s, q FROM Subject s JOIN Question q on s.id = q.subject group by s";
//        TypedQuery<DtoQuestion> cust = entityManager.createQuery(cq, DtoQuestion.class);
//        List<DtoQuestion> qs = cust.setMaxResults(10).getResultList();

//        String joinQuery = "select s, q from Question q join q.subject s where q.subject = 1";
//        String joinQuery = "select s, q from Subject s where s.category = "+ categoryId +" join q.subject";
        // TypedQuery<Object[]> cust2 = entityManager.createQuery(cq, Object[].class);
        // List<Object[]> qs2 = cust2.getResultList();


        // List<Subject> questions = s.setMaxResults(Integer.parseInt(map.get("limit"))).getResultList();


//        questions.forEach(question -> {
//            question.getCategory().setSubjects(new ArrayList<>());
//            question.getSubject().setSections(new ArrayList<>());
//            question.getSubject().setCategories(new ArrayList<>());
//            question.getSection().setTopics(new ArrayList<>());
//            question.getSection().setSubjects(new ArrayList<>());
//            question.getTopic().setSections(new ArrayList<>());
//            question.getOptions().forEach(option -> option.setQuestion(null));
//        });


        // List<DtoQuestion> questions = s.getResultList();


    }

    @Override
    public ResponseEntity<Map<String, Object>> filter(List<DtoSubjectWithQuestion> dtoSubjectWithQuestions) {

        dtoSubjectWithQuestions.forEach(dtoSubjectWithQuestion -> {
            TypedQuery<Subject> s = entityManager.createQuery("select s from Subject s where s.id = " + dtoSubjectWithQuestion.getId(), Subject.class);
            Subject subject = s.getResultList().get(0);
            dtoSubjectWithQuestion.setName(subject.getName());

            String questionId = "";
            boolean isFast = true;

            for (DtoQuestion item : dtoSubjectWithQuestion.getQuestions()
            ) {
                if (isFast) {
                    questionId = questionId + item.getId();
                    isFast = false;
                } else {
                    questionId = questionId + ", " + item.getId();
                }
            }
            TypedQuery<Question> q = entityManager.createQuery("select q from Question q where q.id IN(" + questionId + ")", Question.class);
            List<Question> questions = q.getResultList();

            for (int i = 0; i < questions.size(); i++) {
                List<DtoOption> options = new ArrayList<>();
                questions.get(i).getOptions().forEach(option -> {
                    options.add(new DtoOption(option.getId(), option.getAnswer(), option.getCorrect()));
                });
                dtoSubjectWithQuestion.getQuestions().get(i).setTitle(questions.get(i).getTitle());
                dtoSubjectWithQuestion.getQuestions().get(i).setSectionId(questions.get(i).getSection().getId());
                dtoSubjectWithQuestion.getQuestions().get(i).setSectionName(questions.get(i).getSection().getName());
                dtoSubjectWithQuestion.getQuestions().get(i).setTopicId(questions.get(i).getTopic().getId());
                dtoSubjectWithQuestion.getQuestions().get(i).setTopicName(questions.get(i).getTopic().getName());
                dtoSubjectWithQuestion.getQuestions().get(i).setOptions(options);
            }


        });

        return new ResponseModel(HttpStatus.OK
                , "Question all fetch successfully"
                , true)
                .setData(dtoSubjectWithQuestions)
                .build();
    }


//    @Override
//    public ResponseEntity<Map<String, Object>> filter(String categoryId, String subjects,String limit) {
//
//        TypedQuery<Subject> s = entityManager.createQuery("select s from Subject s where s.id IN("+ subjects +")", Subject.class);
//        List<DtoSubjectWithQuestion> subjectWithQuestions = new ArrayList<>();
//        s.getResultList().forEach(subject -> {
//
//            List<DtoQuestion> dtoQuestions = new ArrayList<>();
//            TypedQuery<Question> q = entityManager.createQuery("select q from Question q where q.subject = " +
//                    subject.getId() + " and q.category.id = :categoryId  ORDER BY random()", Question.class);
//            q.setMaxResults(Integer.parseInt(limit)).setParameter("categoryId", Long.parseLong(categoryId)).getResultList().forEach(question -> {
//                List<DtoOption> options = new ArrayList<>();
//                question.getOptions().forEach(option -> {
//                    options.add(new DtoOption(option.getId(), option.getAnswer(), option.getCorrect()));
//                });
//
//                dtoQuestions.add(new DtoQuestion(
//                        question.getId(),
//                        question.getTitle(),
//                        question.getSection().getId(),
//                        question.getSection().getName(),
//                        question.getTopic().getId(),
//                        question.getTopic().getName(),
//                        options));
//
//            });
//            subjectWithQuestions.add(new DtoSubjectWithQuestion(subject.getId(), subject.getName(), dtoQuestions));
//
//        });
//
//        return new ResponseModel(HttpStatus.OK
//                , "Question all fetch successfully"
//                , true)
//                .setData(subjectWithQuestions)
//                .build();
//    }
}
