package com.example.question.question_service.service.subject;

import com.example.question.question_service.model.Category;
import com.example.question.question_service.model.Subject;
import org.springframework.http.ResponseEntity;

import java.util.Map;

public interface ISubjectService {
    ResponseEntity<Map<String ,Object>> save(Subject subject);
    ResponseEntity<Map<String ,Object>> getAll();
}
