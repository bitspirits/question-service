package com.example.question.question_service.service.category;

import com.example.question.question_service.model.Category;
import com.example.question.question_service.repository.ICategoryRepository;
import com.example.question.question_service.utils.ResponseModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class CategoryService implements ICategoryService{

    @Autowired
    private ICategoryRepository repository;

    @Override
    public ResponseEntity<Map<String, Object>> save(Category category) {
        if(!repository.findByName(category.getName()).isPresent()){
            return new ResponseModel(HttpStatus.OK
                    , "Category save successfully"
                    , true)
                    .setData(repository.save(category))
                    .build();
        }else {
            return new ResponseModel(HttpStatus.OK
                    , "Category Name Already Exist"
                    , false)
                    .build();
        }
    }

    @Override
    public ResponseEntity<Map<String, Object>> getAll() {
        List<Category> categories = repository.findAll();
        categories.forEach(category -> {
            category.getSubjects().forEach(subject -> {
                subject.setCategories(new ArrayList<>());
                subject.getSections().forEach(section -> {
                    section.setSubjects(new ArrayList<>());
                    section.getTopics().forEach(topic -> {
                        topic.setSections(new ArrayList<>());
                    });
                });
            });
        });
       return new ResponseModel(HttpStatus.OK
                , "Category found"
                , true)
                .setData(categories)
                .build();
    }

    @Override
    public ResponseEntity<Map<String, Object>> getCategoryById(String id) {
        Optional<Category> category = repository.findById(Long.parseLong(id));

        category.get().getSubjects().forEach(subject -> {
            subject.setCategories(new ArrayList<>());
            subject.getSections().forEach(section -> {
                section.setSubjects(new ArrayList<>());
                section.getTopics().forEach(topic -> {
                    topic.setSections(new ArrayList<>());
                });
            });
        });

        if(category.isPresent()){
            return new ResponseModel(HttpStatus.OK
                    , "Category found"
                    , true)
                    .setData(category.get())
                    .build();
        }else {
            return new ResponseModel(HttpStatus.OK
                    , "Category not found"
                    , false)
                    .build();
        }
    }
}
