package com.example.question.question_service.service.topic;

import com.example.question.question_service.model.Category;
import com.example.question.question_service.model.Topic;
import org.springframework.http.ResponseEntity;

import java.util.Map;

public interface ITopicService {
    ResponseEntity<Map<String ,Object>> save(Topic topic);
    ResponseEntity<Map<String ,Object>> getAll();
}
