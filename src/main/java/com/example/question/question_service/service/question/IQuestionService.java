package com.example.question.question_service.service.question;

import com.example.question.question_service.dto.DtoSubjectWithQuestion;
import com.example.question.question_service.model.Category;
import com.example.question.question_service.model.Question;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

public interface IQuestionService {
    ResponseEntity<Map<String ,Object>> save(Question question);
    ResponseEntity<Map<String ,Object>> findById(Long id);
    ResponseEntity<Map<String ,Object>> getAll();
    ResponseEntity<Map<String ,Object>> filter(String categoryId, Map<String ,String > map);
    ResponseEntity<Map<String ,Object>> filter(List<DtoSubjectWithQuestion> subjectWithQuestions);

    //ResponseEntity<Map<String ,Object>> filter(String categoryId,String subject,String limit);
}
