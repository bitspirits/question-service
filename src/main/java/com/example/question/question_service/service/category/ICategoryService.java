package com.example.question.question_service.service.category;

import com.example.question.question_service.model.Category;
import org.springframework.http.ResponseEntity;

import java.util.Map;

public interface ICategoryService {
    ResponseEntity<Map<String ,Object>> save(Category category);
    ResponseEntity<Map<String ,Object>> getAll();
    ResponseEntity<Map<String ,Object>> getCategoryById(String id);
}
