package com.example.question.question_service.service.subject;

import com.example.question.question_service.model.Subject;
import com.example.question.question_service.repository.ISubjectRepository;
import com.example.question.question_service.utils.ResponseModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class SubjectService implements ISubjectService{

    @Autowired
    private ISubjectRepository repository;

    @Override
    public ResponseEntity<Map<String, Object>> save(Subject subject) {
        if(!repository.findByName(subject.getName()).isPresent()){
            return new ResponseModel(HttpStatus.OK
                    , "Subject save successfully"
                    , true)
                    .setData(repository.save(subject))
                    .build();
        }else {
            return new ResponseModel(HttpStatus.OK
                    , "Subject Name Already Exist"
                    , false)
                    .build();
        }
    }

    @Override
    public ResponseEntity<Map<String, Object>> getAll() {
        List<Subject> subjects = repository.findAll();
        subjects.forEach(subject -> {
            subject.getCategories().forEach(category -> {
                category.setSubjects(new ArrayList<>());
            });
            subject.getSections().forEach(section -> {
                section.setSubjects(new ArrayList<>());
                section.setTopics(new ArrayList<>());
            });
        });
        return new ResponseModel(HttpStatus.OK
                , "Fetch all subject"
                , true)
                .setData(subjects)
                .build();
    }
}
