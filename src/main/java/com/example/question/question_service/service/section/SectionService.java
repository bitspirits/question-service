package com.example.question.question_service.service.section;

import com.example.question.question_service.model.Category;
import com.example.question.question_service.model.Section;
import com.example.question.question_service.repository.ISectionRepository;
import com.example.question.question_service.utils.ResponseModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class SectionService implements ISectionService{

    @Autowired
    private ISectionRepository repository;

    @Override
    public ResponseEntity<Map<String, Object>> save(Section section) {
        if(!repository.findByName(section.getName()).isPresent()){
            return new ResponseModel(HttpStatus.OK
                    , "Section save successfully"
                    , true)
                    .setData(repository.save(section))
                    .build();
        }else {
            return new ResponseModel(HttpStatus.OK
                    , "Section Name Already Exist"
                    , false)
                    .build();
        }
    }

    @Override
    public ResponseEntity<Map<String, Object>> getAll() {
        List<Section> sections = repository.findAll();
        sections.forEach(section -> {
        section.getSubjects().forEach(subject -> subject.setSections(new ArrayList<>()));
        section.getSubjects().forEach(subject -> subject.setCategories(new ArrayList<>()));
        section.getTopics().forEach(topic -> topic.setSections(new ArrayList<>()));
        });
        return new ResponseModel(HttpStatus.OK
                , "Category save successfully"
                , true)
                .setData(sections)
                .build();
    }
}
