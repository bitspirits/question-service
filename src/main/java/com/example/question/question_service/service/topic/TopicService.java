package com.example.question.question_service.service.topic;

import com.example.question.question_service.model.Section;
import com.example.question.question_service.model.Topic;
import com.example.question.question_service.repository.ITopicRepository;
import com.example.question.question_service.utils.ResponseModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class TopicService implements ITopicService{

    @Autowired
    private ITopicRepository repository;

    @Override
    public ResponseEntity<Map<String, Object>> save(Topic topic) {
        if(!repository.findByName(topic.getName()).isPresent()){
            return new ResponseModel(HttpStatus.OK
                    , "Topic save successfully"
                    , true)
                    .setData(repository.save(topic))
                    .build();
        }else {
            return new ResponseModel(HttpStatus.OK
                    , "Topic Name Already Exist"
                    , false)
                    .build();
        }
    }

    @Override
    public ResponseEntity<Map<String, Object>> getAll() {
        List<Topic> topics = repository.findAll();
        topics.forEach(topic -> {
            topic.getSections().forEach(section -> {
                section.setSubjects(new ArrayList<>());
                section.setTopics(new ArrayList<>());
            });

        });
        return new ResponseModel(HttpStatus.OK
                , "Category save successfully"
                , true)
                .setData(topics)
                .build();
    }
}
