package com.example.question.question_service.repository;

import com.example.question.question_service.model.Section;
import com.example.question.question_service.model.Subject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

public interface ISectionRepository extends JpaRepository<Section, Long> {
    Optional<Section> findByName(String name);
}
