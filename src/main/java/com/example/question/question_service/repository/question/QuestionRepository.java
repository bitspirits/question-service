package com.example.question.question_service.repository.question;


import com.example.question.question_service.model.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import java.util.Map;

@Repository("question_repository")
public class QuestionRepository implements ICustomQuestionRepository{

    @PersistenceContext
    private EntityManager em;

    //@Override
    public Query filter(Map<String ,String > map) {

        String baseQuery = "select * from questions q where q.category.id = 1 and q.subject.id = 1 and q.section.id = 2 and q.topic.id = 1";
        String endPointQuery = "";

        endPointQuery = map.containsKey("category") ? endPointQuery + "q.category.id = " + map.get("category") : endPointQuery;
        endPointQuery = map.containsKey("subject") ? endPointQuery + " and q.subject.id = " + map.get("subject") : endPointQuery;
        endPointQuery = map.containsKey("section") ? endPointQuery + " and q.section.id = " + map.get("section") : endPointQuery;
        endPointQuery = map.containsKey("topic") ? endPointQuery + " and q.topic.id = " + map.get("topic") : endPointQuery;


        return em.createNativeQuery(baseQuery);
    }
}
