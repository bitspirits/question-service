package com.example.question.question_service.repository;

import com.example.question.question_service.model.Option;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IOptionRepository extends JpaRepository<Option, Long> {
}
