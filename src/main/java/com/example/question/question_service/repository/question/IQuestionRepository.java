package com.example.question.question_service.repository.question;

import com.example.question.question_service.model.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface IQuestionRepository extends JpaRepository<Question, Long> {
    Optional<Question> findByTitle(String title);

    @Query(value = "select * from questions q where q.category = 1 and q.subject = 1 and q.section = 2 and q.topic = CAST (:endpoint as Integer)", nativeQuery = true)
    List<Question> findQuestionsByCategoryAndSubjectAndSectionAndTopic(@Param("endpoint") String endPoint);
}
