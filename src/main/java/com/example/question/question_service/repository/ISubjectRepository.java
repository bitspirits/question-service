package com.example.question.question_service.repository;

import com.example.question.question_service.model.Subject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import java.util.Optional;

public interface ISubjectRepository extends JpaRepository<Subject, Long> {
    Optional<Subject> findByName(String name);
}
